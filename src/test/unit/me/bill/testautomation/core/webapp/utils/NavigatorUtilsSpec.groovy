package me.bill.testautomation.core.webapp.utils

import geb.navigator.Navigator
import me.bill.testautomation.core.webapp.utils.NavigatorUtils
import org.openqa.selenium.WebElement
import spock.lang.Specification

class NavigatorUtilsSpec extends Specification {

	Navigator navigator = Mock(Navigator)
	WebElement element = Mock(WebElement)

	void "Test clearInputIfNotEmpty()"() {
		when:
		1 * navigator.value() >> "value"
		1 * navigator.firstElement() >> element
		1 * element.clear()

		then:
		NavigatorUtils.clearInputIfNotEmpty(navigator)
	}

	void "Test checkAllCheckboxes()"() {
		when:
		2 * navigator.allElements() >> [element]
		1 * element.click()
		element.isSelected() >> false

		then:
		NavigatorUtils.checkAllCheckboxes(navigator)
	}

	void "Test uncheckAllCheckboxes"() {
		when:
		2 * navigator.allElements() >> [element]
		1 * element.click()
		element.isSelected() >> true

		then:
		NavigatorUtils.uncheckAllCheckboxes(navigator)
	}
}
