package me.bill.testautomation.core.webapp.utils

import me.bill.testautomation.core.webapp.utils.AppUtils
import spock.lang.Specification

class AppUtilsSpec extends Specification {

	void setup() {
		GroovyMock(System, global: true)
	}

	void "Test getConfigEnvironment()"() {
		when:
		AppUtils.getConfigEnvironment() == env

		then:
		System.getProperty("geb.env") >> value

		where:
		value                 || env
		"sandboxTest"         || "sandbox"
		"sandboxHeadlessTest" || "sandbox"
		"sandboxMobileTest"   || "sandbox"
		"stageTest"           || "stage"
		"stageHeadlessTest"   || "stage"
		"stageMobileTest"     || "stage"
		"devTest"             || "dev"
		"devHeadlessTest"     || "dev"
	}

	void "Test isHeadlessModeOn()"() {
		when:
		AppUtils.isHeadlessModeOn() == mode

		then:
		System.getProperty("geb.env") >> value

		where:
		value                 || mode
		"sandboxTest"         || Boolean.FALSE
		"sandboxHeadlessTest" || Boolean.TRUE
		"sandboxMobileTest"   || Boolean.FALSE
		"stageTest"           || Boolean.FALSE
		"stageHeadlessTest"   || Boolean.TRUE
		"stageMobileTest"     || Boolean.FALSE
		"devTest"             || Boolean.FALSE
		"devHeadlessTest"     || Boolean.FALSE
	}
}
