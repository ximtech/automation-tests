package me.bill.testautomation.core.webapp.utils

import me.bill.testautomation.core.webapp.utils.AppUtils
import me.bill.testautomation.core.webapp.utils.FileUtils
import spock.lang.Specification

class FileUtilsSpec extends Specification {

	void setup() {
		GroovyMock(AppUtils, global: true)
		AppUtils.getConfigEnvironment() >> "test"
	}

	void "Test getConfigProperty() for correct value and encoding"() {
		expect:
		FileUtils.getConfigProperty(key) == value

		where:
		key              || value
		"test.key.one"   || "Rīga"
		"test.key.two"   || "Ābolu iela"
		"test.key.three" || "Latin symbol string"
		"test.key.four"  || "Latin symbol string with digits 1234"
	}

	void "Test findFileInClasspathByName() for correct file search"() {
		when:
		File result = FileUtils.findFileInClasspathByName("/config.test.properties")

		then:
		notThrown(IllegalArgumentException)
		result.getName() == "config.test.properties"
	}
}
