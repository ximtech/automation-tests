package me.bill.testautomation.core.webapp.customer

import me.bill.testautomation.core.webapp.BaseTestSpec
import me.bill.testautomation.core.webapp.pages.customer.CustomerDashboardPage
import me.bill.testautomation.core.webapp.pages.customer.CustomerPage
import me.bill.testautomation.core.webapp.pages.customer.CustomerServicesPage
import me.bill.testautomation.core.webapp.pages.login.LoginPage

import static me.bill.testautomation.core.webapp.data.AppData.*
import static me.bill.testautomation.core.webapp.modules.customer.CustomerPageTab.SERVICES
import static me.bill.testautomation.core.webapp.modules.customer.services.ServicesTab.TENANTS

class ServicesSpec extends BaseTestSpec {

	void "Test add/remove tenant to/from apartment"() {
		when: "Login and go to services page"
		LoginPage loginPage = to LoginPage
		loginPage."Sign up"(BME_CUSTOMER_USER_DEFAULT_ACCOUNT)

		CustomerPage dashboardPage = at CustomerDashboardPage
		dashboardPage."Click on tab"(SERVICES)

		and: "Choose company and click on tab tenants"
		CustomerServicesPage servicesPage = at CustomerServicesPage
		servicesPage."Click on company tab by it name"(BME_COMPANY_DEFAULT_NAME)
		servicesPage."Click on Services tab"(TENANTS)

		then: "Add and then remove tenant from apartment"
		servicesPage."Add tenant to apartment"(BME_TENANT_DEFAULT_ACCOUNT)
		servicesPage."Remove tenant from apartment"(BME_TENANT_DEFAULT_ACCOUNT)
		at CustomerServicesPage
	}
}
