package me.bill.testautomation.core.webapp.login

import me.bill.testautomation.core.webapp.BaseTestSpec
import me.bill.testautomation.core.webapp.pages.GoogleMailPage
import me.bill.testautomation.core.webapp.pages.customer.CustomerDashboardPage
import me.bill.testautomation.core.webapp.pages.login.ForgetPasswordPage
import me.bill.testautomation.core.webapp.pages.login.LoginPage
import me.bill.testautomation.core.webapp.pages.login.ResetPasswordPage

import static me.bill.testautomation.core.webapp.data.AppData.BME_CUSTOMER_USER_DEFAULT_ACCOUNT
import static me.bill.testautomation.core.webapp.data.AppData.BME_CUSTOMER_USER_MAIL_DEFAULT_ACCOUNT

class PasswordRecoverySpec extends BaseTestSpec {

	void "Test password recovery"() {
		when: "Start the password change request"
		ForgetPasswordPage forgetPasswordPage = to ForgetPasswordPage
		forgetPasswordPage."Request for password change"(BME_CUSTOMER_USER_MAIL_DEFAULT_ACCOUNT.username)

		and: report("Go to mail and find password reset mail then proceed to password reset page")
		GoogleMailPage mailPage = to GoogleMailPage
		mailPage."Sign up to google mail account"(BME_CUSTOMER_USER_MAIL_DEFAULT_ACCOUNT)
		report("Sign up to client google mail account")

		and: report("In client mail account")
		mailPage."Open reset password incoming message"()

		and: report("Password recovery mail")
		mailPage."Click on link for password reset"()

		and: report("Change user password")
		ResetPasswordPage resetPasswordPage = at ResetPasswordPage
		resetPasswordPage."Reset password with new data"(BME_CUSTOMER_USER_DEFAULT_ACCOUNT.password)

		then: report("Validate that password successfully updated")
		LoginPage loginPage = to LoginPage
		loginPage."Sign up"(BME_CUSTOMER_USER_DEFAULT_ACCOUNT)
		at CustomerDashboardPage
	}
}
