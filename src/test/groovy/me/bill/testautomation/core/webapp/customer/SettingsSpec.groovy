package me.bill.testautomation.core.webapp.customer

import me.bill.testautomation.core.webapp.BaseTestSpec
import me.bill.testautomation.core.webapp.data.AppData
import me.bill.testautomation.core.webapp.pages.customer.CustomerDashboardPage
import me.bill.testautomation.core.webapp.pages.customer.CustomerSettingsPage
import me.bill.testautomation.core.webapp.pages.login.LoginPage

import static me.bill.testautomation.core.webapp.modules.customer.CustomerPageTab.DASHBOARD
import static me.bill.testautomation.core.webapp.modules.customer.PageTitleName.SETTINGS
import static me.bill.testautomation.core.webapp.modules.customer.PageTitleName.WELCOME
import static me.bill.testautomation.core.webapp.pages.PageLanguage.ENGLISH
import static me.bill.testautomation.core.webapp.pages.PageLanguage.RUSSIAN

class SettingsSpec extends BaseTestSpec {

	void "Test customer page language change"() {
		when: "Change language at login page"
		LoginPage loginPage = to LoginPage
		loginPage."Set login page language"(RUSSIAN)
		loginPage."Sign up"(AppData.BME_CUSTOMER_USER_DEFAULT_ACCOUNT)

		then: "Validate that customer dashboard language changed"
		CustomerDashboardPage dashboardPage = at CustomerDashboardPage
		dashboardPage."Validate page title"(WELCOME.getRussianSpelling())

		when: "Change language to English"
		CustomerSettingsPage settingsPage = to CustomerSettingsPage
		settingsPage."Change page language"(ENGLISH)
		settingsPage."Validate page title"(SETTINGS.getEnglishSpelling())

		then: "Validate customer dashboard language"
		settingsPage."Click on tab"(DASHBOARD)
		dashboardPage."Validate page title"(WELCOME.getEnglishSpelling())
		at CustomerDashboardPage
	}

	void "Test customer settings password and phone number changing"() {
		when: "Change language from customer settings page"
		CustomerSettingsPage settingsPage = to CustomerSettingsPage
		settingsPage."Change page language"(ENGLISH)
		settingsPage."Validate page title"(SETTINGS.getEnglishSpelling())

		and: "Enter incorrect phone number"
		settingsPage."Enter phone number"(INVALID_PHONE_NUMBER)
		settingsPage."Save phone number"()
		assert settingsPage."Is error data not saved message is displayed"() : "Invalid phone number can't be saved"

		and: "Enter correct phone number"
		settingsPage."Enter phone number"(VALID_PHONE_NUMBER)
		settingsPage."Save phone number"()
		assert settingsPage."Is success data saved message is displayed"() : "Correct phone number should be successfully saved"

		and: "Enter weak password"
		settingsPage."Click on password change block"()
		settingsPage."Enter new password"(WEAK_PASSWORD)
		settingsPage."Confirm new password"(WEAK_PASSWORD)
		settingsPage."Click to save new password button"()
		assert settingsPage."Is error data not saved message is displayed"() : "Weak password can't be saved"

		and: "Enter wrong confirm new password"
		settingsPage."Enter new password"(VALID_PASSWORD)
		settingsPage."Confirm new password"(WEAK_PASSWORD)
		settingsPage."Click to save new password button"()
		assert settingsPage."Is error data not saved message is displayed"() : "Incorrect password confirmation can't be saved"

		and: "Enter correct password"
		settingsPage."Enter new password"(VALID_PASSWORD)
		settingsPage."Confirm new password"(VALID_PASSWORD)
		settingsPage."Click to save new password button"()
		assert settingsPage."Is success data saved message is displayed"() : "Valid password should be successfully saved"

		then:
		at CustomerSettingsPage
	}

	void "Test customer settings notification checking"() {
		when:
		CustomerSettingsPage settingsPage = to CustomerSettingsPage
		settingsPage."Go to Notifications"()
		settingsPage."Uncheck all notification checkboxes"()
		settingsPage."Click to save notifications button"()
		assert settingsPage."Is success data saved message is displayed"() : "Customer with disabled notification data should be saved"

		and:
		settingsPage."Check all notification checkboxes"()
		settingsPage."Click to save notifications button"()
		assert settingsPage."Is success data saved message is displayed"() : "Customer with full enabled notification data should be saved"

		then:
		at CustomerSettingsPage
	}
}
