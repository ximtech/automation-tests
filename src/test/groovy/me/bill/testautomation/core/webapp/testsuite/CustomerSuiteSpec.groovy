package me.bill.testautomation.core.webapp.testsuite


import me.bill.testautomation.core.webapp.customer.HelpSpec
import me.bill.testautomation.core.webapp.customer.SettingsSpec
import me.bill.testautomation.core.webapp.customer.ServicesSpec
import me.bill.testautomation.core.webapp.login.PasswordRecoverySpec
import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite)
@Suite.SuiteClasses([
		SettingsSpec,
		HelpSpec,
		ServicesSpec,
		PasswordRecoverySpec
])
class CustomerSuiteSpec {}
