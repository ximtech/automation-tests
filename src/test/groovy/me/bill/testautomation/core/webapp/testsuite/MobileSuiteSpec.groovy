package me.bill.testautomation.core.webapp.testsuite

import me.bill.testautomation.core.webapp.mobile.customer.MobileSpec
import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite)
@Suite.SuiteClasses([
		MobileSpec
])
class MobileSuiteSpec {
}
