package me.bill.testautomation.core.webapp.customer

import me.bill.testautomation.core.webapp.BaseTestSpec
import me.bill.testautomation.core.webapp.pages.customer.CustomerDashboardPage
import me.bill.testautomation.core.webapp.pages.customer.CustomerHelpPage
import me.bill.testautomation.core.webapp.pages.customer.CustomerPage
import me.bill.testautomation.core.webapp.pages.login.LoginPage
import me.bill.testautomation.core.webapp.utils.AppUtils

import static me.bill.testautomation.core.webapp.data.AppData.BME_CUSTOMER_USER_DEFAULT_ACCOUNT
import static me.bill.testautomation.core.webapp.modules.customer.PageTitleName.HELP

class HelpSpec extends BaseTestSpec {

	void "Test contact bill.me form"() {
		when: "Login and go to help page"
		LoginPage loginPage = to LoginPage
		loginPage."Sign up"(BME_CUSTOMER_USER_DEFAULT_ACCOUNT)

		then:
		CustomerPage dashboardPage = at CustomerDashboardPage
		dashboardPage."Go to customer help page"()

		when:
		CustomerHelpPage helpPage = at CustomerHelpPage
		helpPage."Validate page title"(HELP.getEnglishSpelling())

		helpPage."Write message"(AppUtils.getRandomText(10))
		helpPage."Press send button"()
		assert helpPage."Validate that error message has been displaying"() : "Message can't be send without subject"

		and:
		helpPage."Write subject"(AppUtils.getRandomText(4))
		helpPage."Press send button"()
		assert helpPage."Validate that error message has been displaying"() : "Message can't be send without subject"

		and:
		helpPage."Write subject"(AppUtils.getRandomText(4))
		helpPage."Write message"(AppUtils.getRandomText(10))
		helpPage."Press send button"()
		assert helpPage."Validate that message successfully has been send"() : "Message should be successfully send to bill.me support"

		then:
		at CustomerHelpPage
	}
}
