package me.bill.testautomation.core.webapp

import me.bill.testautomation.core.webapp.data.AppData
import geb.driver.CachingDriverFactory
import geb.spock.GebReportingSpec
import spock.lang.Stepwise

@Stepwise
class BaseTestSpec extends GebReportingSpec {
	public static final String VALID_PHONE_NUMBER = "22222222"
	public static final String INVALID_PHONE_NUMBER = "12345"
	public static final String VALID_PASSWORD = AppData.BME_CUSTOMER_USER_DEFAULT_ACCOUNT.password
	public static final String WEAK_PASSWORD = "12345"

	void cleanupSpec() {
		CachingDriverFactory.clearCacheAndQuitDriver()
	}
}
