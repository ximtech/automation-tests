package me.bill.testautomation.core.webapp.factory

import me.bill.testautomation.core.webapp.factory.DriverFactory
import org.openqa.selenium.chrome.ChromeDriver
import spock.lang.Specification

class DriverFactorySpec extends Specification {

	ChromeDriver chromeDriver
	ChromeDriver headlessChromeDriver
	ChromeDriver mobileChromeDriver

	void "Test chrome driver creation"() {
		when:
		chromeDriver = DriverFactory.configureChromeDriver()

		then:
		chromeDriver != null
		Map<String, Object> capabilities = chromeDriver.getCapabilities().asMap()
		testDriverCapabilities(capabilities)
		capabilities.get("mobileEmulationEnabled") == Boolean.FALSE
	}

	void "Test headless chrome driver creation"() {
		when:
		headlessChromeDriver = DriverFactory.configureHeadlessChromeDriver()

		then:
		headlessChromeDriver != null
		Map<String, Object> capabilities = headlessChromeDriver.getCapabilities().asMap()
		testDriverCapabilities(capabilities)
		capabilities.get("mobileEmulationEnabled") == Boolean.FALSE
	}

	void "Test geb.automation.mobile chrome driver creation"() {
		when:
		mobileChromeDriver = DriverFactory.configureMobileChromeDriver()

		then:
		mobileChromeDriver != null
		Map<String, Object> capabilities = mobileChromeDriver.getCapabilities().asMap()
		testDriverCapabilities(capabilities)
		capabilities.get("mobileEmulationEnabled") == Boolean.TRUE
	}

	private static void testDriverCapabilities(Map<String, Object> capabilities) {
		capabilities.get("browserName") == "chrome"
		capabilities.get("version") == "74.0.3729.169"
		capabilities.get("acceptInsecureCerts") == Boolean.FALSE
		capabilities.get("takesScreenshot") == Boolean.TRUE
	}

	void cleanup() {
		if (chromeDriver) {
			chromeDriver.close()
			chromeDriver.quit()
		}
		if (headlessChromeDriver) {
			headlessChromeDriver.close()
			headlessChromeDriver.quit()
		}
		if (mobileChromeDriver) {
			mobileChromeDriver.close()
			mobileChromeDriver.quit()
		}
	}
}
