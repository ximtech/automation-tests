package me.bill.testautomation.core.webapp.scripts

import geb.Browser
import me.bill.testautomation.core.webapp.data.AppData
import me.bill.testautomation.core.webapp.pages.customer.CustomerDashboardPage
import me.bill.testautomation.core.webapp.pages.customer.CustomerSettingsPage
import me.bill.testautomation.core.webapp.pages.login.LoginPage

import static me.bill.testautomation.core.webapp.pages.PageLanguage.ENGLISH
import static me.bill.testautomation.core.webapp.pages.PageLanguage.RUSSIAN
import static me.bill.testautomation.core.webapp.modules.customer.PageTitleName.WELCOME

Browser.drive {

    to LoginPage
    "Set login page language"(RUSSIAN)
    "Sign up"(AppData.BME_CUSTOMER_USER_DEFAULT_ACCOUNT)

    at CustomerDashboardPage
    "Validate page title"(WELCOME.getRussianSpelling())

	to CustomerSettingsPage
	"Change page language"(ENGLISH)

	"Enter phone number"("22556644")
	"Save phone number"()
	"Validate that phone number is saved"()

	"Enter phone number"("123")
	"Save phone number"()
	"Validate that phone number is not saved"()

	"Click on password change block"()
	"Enter new password"("123")
	"Confirm new password"("123")
	"Click to save new password button"()
	"Validate that password is not saved"()

	"Enter new password"("Billme12")
	"Confirm new password"("Billme122")
	"Click to save new password button"()
	"Validate that password is not saved"()

	"Enter new password"("Billme12")
	"Confirm new password"("Billme12")
	"Click to save new password button"()
	"Validate that password is saved"()


	/*"Click on tab"(DASHBOARD)

	at CustomerDashboardPage
    "Validate page title"(WELCOME.getEnglishSpelling())
	"Open incoming notification by it's name"("NNNN")
    "Go to customer settings page"()
    "Go to customer help page"()

	"Logout"()*/

}