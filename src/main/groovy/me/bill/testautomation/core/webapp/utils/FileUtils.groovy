package me.bill.testautomation.core.webapp.utils

import jodd.io.FileUtil
import org.apache.commons.lang3.SystemUtils

import java.nio.charset.StandardCharsets

class FileUtils {
    private static final String CONFIGURATION_PROPERTIES_PATH = String.format("config.%s.properties", AppUtils.getConfigEnvironment())
    private static final String WIN_CHROME_DRIVER = "/drivers/chrome/chromedriver_win32.exe"
    private static final String LINUX_CHROME_DRIVER = "/drivers/chrome/chromedriver_linux64"
    private static final String MAC_CHROME_DRIVER = "/drivers/chrome/chromedriver_mac64"

    private static class SingletonProperties {
        private static final Properties CONFIG_PROPERTIES = initializeConfigProperties(CONFIGURATION_PROPERTIES_PATH)
    }

    static String getConfigProperty(String key) {
        return SingletonProperties.CONFIG_PROPERTIES.getProperty(key)
    }

    static Properties initializeConfigProperties(String configFileName) {
        URL url = Thread.currentThread().getContextClassLoader().getResource(configFileName)
        validateThatFileExists(configFileName, url)
        File configPropertiesFile = new File(url.getPath())
        FileInputStream fileInputStream = new FileInputStream(configPropertiesFile)
        InputStreamReader reader = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8)
        Properties configProperties = new Properties()
        configProperties.load(reader)
        return configProperties
    }

    static File findChromeDriverByOsType() {
        String driverFileName = resolveChromeDriverNameByOSType()
        return findFileInClasspathByName(driverFileName)
    }

    static File findFileInClasspathByName(String fileName) {
        URL url = FileUtil.getResource(fileName)
        validateThatFileExists(fileName, url)
        File file = new File(url.getPath())
        return file
    }

    private static String resolveChromeDriverNameByOSType() {
        if (SystemUtils.IS_OS_WINDOWS) {
            return WIN_CHROME_DRIVER
        } else if (SystemUtils.IS_OS_LINUX) {
            return LINUX_CHROME_DRIVER
        } else if (SystemUtils.IS_OS_MAC) {
            return MAC_CHROME_DRIVER
        } else {
            throw new RuntimeException("Unsupported OS type!")
        }
    }

    private static void validateThatFileExists(String fileName, URL url) {
        if (url == null) {
            throw new IllegalArgumentException("File with name: ${fileName} is not found.")
        }
    }
}
