package me.bill.testautomation.core.webapp.modules

import geb.Module
import org.openqa.selenium.By

class HeaderModule extends Module {

	static content = {
		dataIsSaved(cache: true) { $("div.message-block.msg-type-success.active") }
		dataNotSaved(cache: true) { $("div.message-block.msg-type-error.active") }
		logoutButton(wait: true, cache: true) { $(By.xpath("/html/body/section/header/nav/form/button")) }
	}

	boolean "Is success message appears"() {
		return waitFor("quick") { dataIsSaved.isDisplayed() }
	}

	boolean "Is error message appears"() {
		return waitFor("quick") { dataNotSaved.isDisplayed() }
	}

	void "Logout from account"() {
		logoutButton.click()
	}

}
