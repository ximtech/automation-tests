package me.bill.testautomation.core.webapp.pages

enum PageLanguage {
    ENGLISH("English", "EN"),
    LATVIAN("Latviešu", "LV"),
    RUSSIAN("Русский", "RU"),
    ESTONIAN("Eesti", "ET"),
    SPANISH("Español", "ES")

    private String langName
    private String langShortName

    PageLanguage(String langName, String langShortName) {
        this.langName = langName
        this.langShortName = langShortName
    }

    String getLangName() {
        return langName
    }

    String getLangShortName() {
        return langShortName
    }
}
