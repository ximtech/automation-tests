package me.bill.testautomation.core.webapp.modules.customer.services

import geb.Module
import me.bill.testautomation.core.webapp.data.users.TenantAccount
import geb.module.TelInput
import geb.module.TextInput
import org.apache.commons.lang3.StringUtils

class TenantModule extends Module {

    static content = {
        addTenantButton(wait: true) { $("span.service-page__add-user__btn") }
        tenantEmailInput(wait: true) { $("input#add-tenant-username").module(TextInput) }
        addressDropdown() { $("div.smart-chosen-single") }
        confirmAddTenantButton() { $("button.main-button.orange-button.ng-scope") }
        suchUserIsNotExistMessage { $("div.msg-fail").children("h2") }
        createTenantButton(wait: true) { $("button.main-button.orange-button").$("span.ng-binding") }

        tenantFirstNameInput(wait: true) { $("input#add-tenant-firstname").module(TextInput) }
        tenantLastNameInput() { $("input#add-tenant-lastname").module(TextInput) }
        tenantPhoneNumberInput() { $("input#add-tenant-phonenumber").module(TelInput) }
        saveNewTenantButton() { $("button.main-button.orange-button.ng-scope") }
    }

    void "Add tenant to apartment"(TenantAccount tenant) {
        validateTenantData(tenant.email, tenant.address)
        addTenantButton.click()
        tenantEmailInput << tenant.email
        selectFullAddress(tenant.address)
        confirmAddTenantButton.click()

        waitFor("quick") { suchUserIsNotExistMessage }
        if (suchUserIsNotExistMessage.isDisplayed()) {
            createNewTenantForApartment(tenant)
        }
    }

    void "Remove tenant from apartment"(TenantAccount tenant) {
        findTenantByFullName(tenant.getFullName())
        removeTenantFromApartment(tenant.getFullName())
    }

    private void validateTenantData(String tenantEmail, String address) {
        assert StringUtils.isNotBlank(tenantEmail): "Tenant email can't be empty"
        assert StringUtils.isNotBlank(address): "Apartment address can't be empty"
    }

    private void selectFullAddress(String address) {
        addressDropdown.click()
        $("li.active-result", text: contains(address)).click()
    }

    private void validateTenantCreationData(String firstName, String lastName) {
        assert StringUtils.isNotBlank(firstName) : "New tenant creation requires first name"
        assert StringUtils.isNotBlank(lastName) : "New tenant creation requires last name"
    }

    private void createNewTenantForApartment(TenantAccount tenant) {
        createTenantButton.click()
        validateTenantCreationData(tenant.firstName, tenant.lastName)
        tenantFirstNameInput << tenant.firstName
        tenantLastNameInput << tenant.lastName

        if (StringUtils.isNotBlank(tenant.phoneNumber)) {
            tenantPhoneNumberInput << tenant.phoneNumber
        }
        saveNewTenantButton.click()
    }

    private void findTenantByFullName(String tenantFullName) {
        assert StringUtils.isNotBlank(tenantFullName) : "Tenant full name can't be empty"
        assert waitFor("quick") {
            $("div.flippable.service-row-item")
                    .children("div")
                    .children("span.service-card__title", text: contains(tenantFullName))
                    .parent("div")
                    .children("span.service-card__close")
                    .click() } : "Tenant with full name ${tenantFullName} is not found"
    }

    private void removeTenantFromApartment(String tenantFullName) {
        assert waitFor("quick") {
            $("div.flippable.service-row-item.flipped")
                    .$("div.flippable-dialog__content")
                    .children("div.flippable-dialog__buttons")
                    .children("span.btn")
                    .first()
                    .click() } : "Can't remove tenant: ${tenantFullName} from apartment"
    }


}
