package me.bill.testautomation.core.webapp.modules.customer.services

enum ServicesTab {
    COMPANY_INFO(0),
    TENANTS(1),
    DOCUMENTS(2),
    CONTACTS(3),
    NEWS(4),
    FORUMS(5),
    ANNOUNCEMENTS(6),
    METERS(7)

    private int tabNumber

    ServicesTab(int tabNumber) {
        this.tabNumber = tabNumber
    }

    int getTabNumber() {
        return tabNumber
    }
}