package me.bill.testautomation.core.webapp.pages.customer

import me.bill.testautomation.core.webapp.modules.customer.CustomerHeaderModule
import me.bill.testautomation.core.webapp.modules.customer.CustomerPageTab
import me.bill.testautomation.core.webapp.modules.customer.CustomerSideModule
import me.bill.testautomation.core.webapp.pages.BasePage

abstract class CustomerPage extends BasePage {

    static content = {
        customerSideModule { module CustomerSideModule }
        customerHeaderModule { module CustomerHeaderModule }
    }

    abstract void "Validate page title"(String pageTitleName)

    void "Open incoming notification by it's name"(String notificationName) {
        customerHeaderModule."Open incoming notification by name"(notificationName)
    }

    void "Click on tab"(CustomerPageTab tab) {
        customerSideModule."Click on tab"(tab)
    }

    void "Go to customer settings page"() {
        customerHeaderModule."Go to settings page"()
    }

    void "Go to customer help page"() {
        customerHeaderModule."Go to help page"()
    }

}
