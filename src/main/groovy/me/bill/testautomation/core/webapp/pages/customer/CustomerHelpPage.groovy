package me.bill.testautomation.core.webapp.pages.customer

import geb.module.TextInput
import geb.module.Textarea
import me.bill.testautomation.core.webapp.utils.NavigatorUtils

class CustomerHelpPage extends CustomerPage {

	static url = "customerUser/feedback"

	static at = {
		$("h2.chi-content-title").isDisplayed()
	}

	static content = {
		pageTitle(wait: true) { $("h2.chi-content-title") }
		contactSubject(wait: true) { $("input#msgSubject").module(TextInput) }
		contactMessage(wait: true) { $("textarea#msgText").module(Textarea) }
		sendButton() { $("button.main-button.orange-button", type: "submit") }
	}

	void "Write subject"(String subjectText) {
		NavigatorUtils.clearInputIfNotEmpty(contactSubject)
		contactSubject << subjectText
	}

	void "Write message"(String messageText) {
		NavigatorUtils.clearInputIfNotEmpty(contactSubject)
		contactMessage << messageText
	}

	void "Press send button"() {
		sendButton.click()
	}

	boolean "Validate that message successfully has been send"() {
		return "Is success data saved message is displayed"()
	}

	boolean "Validate that error message has been displaying"() {
		return "Is error data not saved message is displayed"()
	}

	@Override
	void "Validate page title"(String pageTitleName) {
		assert pageTitle.text() == pageTitleName: "Customer Help page title is inccorect: [${pageTitle.text()}] is not equals [${pageTitleName}]"
	}
}
