package me.bill.testautomation.core.webapp.utils

import de.svenjacobs.loremipsum.LoremIpsum

class AppUtils {
	public static final String STAGE_ENV = "stage"
	public static final String SANDBOX_ENV = "sandbox"
	public static final String DEV_ENV = "dev"

	static String getConfigEnvironment() {
		String env = getWorkingEnvironment()
		switch (env) {
			case env.matches("^stage\\w*\$"): return STAGE_ENV
			case env.matches("^sandbox\\w*\$"): return SANDBOX_ENV
			case env.matches("^dev\\w*\$"): return DEV_ENV
			default: STAGE_ENV
		}
	}

	static boolean isHeadlessModeOn() {
		return getWorkingEnvironment().matches("^\\w+([hH]eadless)\$")
	}

	static String getRandomText(int wordCount) {
		return new LoremIpsum().getWords(wordCount)
	}

	private static String getWorkingEnvironment() {
		String env = System.getProperty("geb.env")
		return env != null ? env : ""
	}
}
