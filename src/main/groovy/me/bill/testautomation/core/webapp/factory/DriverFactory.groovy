package me.bill.testautomation.core.webapp.factory

import me.bill.testautomation.core.webapp.utils.FileUtils
import org.openqa.selenium.Dimension
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions

import java.util.concurrent.TimeUnit

class DriverFactory {
	public static final String USER_AGENT = "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) " + //FIXME: old user agent, need to update
			"AppleWebKit/535.19 (KHTML, like Gecko) Chrome/74.0.3729.169 Mobile Safari/535.19"

	static ChromeDriver configureChromeDriver() {
		ChromeOptions chromeOptions = doGetChromeOptions()
		def driver = doConfigureChromeDriver(chromeOptions)
		driver.manage().window().maximize()
		return driver
	}

	static ChromeDriver configureHeadlessChromeDriver() {
		ChromeOptions chromeOptions = doGetChromeOptions()
		chromeOptions.setHeadless(true)
		chromeOptions.addArguments("--no-sandbox", "--allow-insecure-localhost")
		def driver = doConfigureChromeDriver(chromeOptions)
		driver.manage().window().setSize(new Dimension(1920, 1080))
		return driver
	}

	static ChromeDriver configureMobileChromeDriver() {
		ChromeOptions chromeOptions = doGetChromeOptions()
		Map<String, Object> deviceMetrics = ["width": 360, "height": 640, "pixelRatio": 3.0] as HashMap
		Map<String, Object> mobileEmulation = ["deviceMetrics": deviceMetrics, "userAgent": USER_AGENT]
		chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation)
		def driver = doConfigureChromeDriver(chromeOptions)
		driver.manage().window().maximize()
		return driver
	}

	private static  ChromeDriver doConfigureChromeDriver(ChromeOptions chromeOptions) {
		File chromeDriver = FileUtils.findChromeDriverByOsType()
		setChromeDriverPathToSystemProperties(chromeDriver.absolutePath)
		def driver = new ChromeDriver(chromeOptions)
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS)
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS)
		return driver
	}

	private static  ChromeOptions doGetChromeOptions() {
		ChromeOptions chromeOptions = new ChromeOptions()
		chromeOptions.addArguments("--disable-notifications", "--lang=en-gb")//set the chrome locale to 'en-GB' since the tests expects english lang
		chromeOptions.setExperimentalOption("prefs", ["intl.accept_languages": "en-GB"])
		return chromeOptions
	}

	private static  void setChromeDriverPathToSystemProperties(String driverAbsolutePath) {
		System.setProperty("webdriver.chrome.driver", driverAbsolutePath)
	}

}
