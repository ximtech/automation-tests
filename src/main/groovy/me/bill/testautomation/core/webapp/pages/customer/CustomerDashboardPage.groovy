package me.bill.testautomation.core.webapp.pages.customer

class CustomerDashboardPage extends CustomerPage {

    static url = "customerUser/dashboard"

    static at = {
        $("h2.chi-content-title").isDisplayed()
    }

    static content = {
        pageTitle(wait: true) { $("h2.chi-content-title") }
    }

    @Override
    void "Validate page title"(String pageTitleName) {
        assert pageTitle.text() == pageTitleName: "Customer dashboard page title is inccorect: [${pageTitle.text()}] is not equals [${pageTitleName}]"
    }
}
