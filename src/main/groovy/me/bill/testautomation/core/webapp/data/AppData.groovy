package me.bill.testautomation.core.webapp.data

import me.bill.testautomation.core.webapp.data.company.Company
import me.bill.testautomation.core.webapp.data.users.Account
import me.bill.testautomation.core.webapp.data.users.AdminAccount
import me.bill.testautomation.core.webapp.data.users.CustomerAccount
import me.bill.testautomation.core.webapp.data.users.MailAccount
import me.bill.testautomation.core.webapp.data.users.TenantAccount

import static me.bill.testautomation.core.webapp.utils.FileUtils.getConfigProperty

class AppData {
    public static final String BME_COMPANY_DEFAULT_NAME = getConfigProperty("bme.company.name")

    public static final AdminAccount BME_ADMIN_USER_DEFAULT_ACCOUNT = new AdminAccount(
            username   : getConfigProperty("bme.admin.username"),
            password   : getConfigProperty("bme.admin.password")
    )

    public static final CustomerAccount BME_CUSTOMER_USER_DEFAULT_ACCOUNT = new CustomerAccount(
            username   : getConfigProperty("bme.companyClient.username"),
            password   : getConfigProperty("bme.companyClient.password"),
            phoneNumber: getConfigProperty("bme.common.phoneNumber1")
    )

    public static final Account BME_CUSTOMER_USER_MAIL_DEFAULT_ACCOUNT = new MailAccount(
            username: getConfigProperty("google.mail.username"),
            password: getConfigProperty("google.mail.password")
    )

    public static final TenantAccount BME_TENANT_DEFAULT_ACCOUNT = new TenantAccount(
            firstName  : getConfigProperty("bme.tenant.firstName"),
            lastName   : getConfigProperty("bme.tenant.lastName"),
            email      : getConfigProperty("bme.tenant.email"),
            address    : getConfigProperty("bme.tenant.address"),
            phoneNumber: getConfigProperty("bme.common.phoneNumber2")
    )

    public static final PROPERTY_MANAGEMENT_COMPANY = new Company(
            name: getConfigProperty("bme.company.name"),        //TODO finish that
            regNumber: "",
            vatRegNumber: "",
            country: "",
            city: "",
            street: "",
            houseNumber: "",
            postalCode: "",
            serviceCommission: "",
            defaultLanguage: "",
            bmeIsGrantedToSendWelcomeMails: "",
            isPropertyManagementCompany: "",
            isMetersCustomerReadingsSubmitAllowed: "",
            infoPhoneNumber: "",
            email: "",
            website: "",
            icon: "",
            logo: "",
    )
}
