package me.bill.testautomation.core.webapp.pages.customer

import me.bill.testautomation.core.webapp.data.users.TenantAccount
import me.bill.testautomation.core.webapp.modules.customer.services.CompanyInfoModule
import me.bill.testautomation.core.webapp.modules.customer.services.ServicesTab
import me.bill.testautomation.core.webapp.modules.customer.services.TenantModule

class CustomerServicesPage extends CustomerPage {

    static url = "services/companyInfo"

    static at = {
        title == "Bill.me"
        pageTitle.isDisplayed()
    }

    static content = {
        companyInfoModule { module CompanyInfoModule }
        tenantModule { module TenantModule }

        pageTitle(wait: true) { $("div.container.page-title").$("h2") }
        availableCompaniesTabs(wait: true) { $("div.swiper-wrapper", 0).children() }  // tabs with available companies
        availableServicesTabs(wait: true) { $("div.swiper-wrapper", 1).children() }  // tabs with available services
        tenantsAtPage() { $("span.service-card__title") }
    }

    void "Click on company tab by it name"(String companyName) {
        availableCompaniesTabs
                .children(text: contains(companyName))
                .click()
    }

    void "Click on Services tab"(ServicesTab tab) {
        availableServicesTabs[tab.getTabNumber()].click()
    }

    void "Add tenant to apartment"(TenantAccount tenant) {
        tenantModule."Add tenant to apartment"(tenant)
        validateThatTenantHasBeenAddedToAddress(tenant)
    }

    void "Remove tenant from apartment"(TenantAccount tenant) {
        validateThatTenantHasBeenAddedToAddress(tenant)
        tenantModule."Remove tenant from apartment"(tenant)
        validateThatTenatHasBeenRemovedFromApartment(tenant.getFullName(), tenant.getEmail())
    }

    @Override
    void "Validate page title"(String pageTitleName) {
        assert pageTitle.text() == pageTitleName: "Customer services page title is inccorect: [${pageTitle.text()}] is not equals [${pageTitleName}]"
    }

    private validateThatTenantHasBeenAddedToAddress(TenantAccount tenant) {
        waitFor("quick") { tenantsAtPage }
        if (!isTenantDisplayed(tenant.getFullName(), tenant.getEmail())) {
            findTenantByAddressThenClickOnAddress(tenant.getAddress())
            assert isTenantDisplayed(tenant.getFullName(), tenant.getEmail()) : "Tenant is not added to address ${tenant.getAddress()}"
        }
    }

    private boolean isTenantDisplayed(String tenantFullName, String email) {
        return $("span.service-card__title", text: contains(tenantFullName))
                .parent()
                .children("ul.service-card__contacts")
                .children("li.service-card__contacts__email", text: contains(email)).isDisplayed()
    }

    private void findTenantByAddressThenClickOnAddress(String address) {
        waitFor("quick") { $("div.accordion-title.js-romt")
                    .children("span.romt-text", text: contains(address))
                    .click() }
    }

    private void validateThatTenatHasBeenRemovedFromApartment(String tenantFullName, String email) {
        assert waitFor("quick") { !isTenantDisplayed(tenantFullName, email) } : "Tenant: ${tenantFullName} is not removed from apartment!"
    }
}
