package me.bill.testautomation.core.webapp.modules.customer

enum PageTitleName {
	WELCOME("Welcome!", "Добро пожаловать!", "Laipni lūdzam!", "Tere tulemast!", "Bienvenido!"),
	SETTINGS("Settings", "Настройки", "Iestatījumi", "Seaded", "Configuraciones"),
	HELP("Contact Bill.me", "Связаться с Bill.me", "Sazināties ar Bill.me", "Võtke meiega ühendust", "Contáctanos"),
	WALLET("Wallet", "Кошелек", "Maks", "Rahakott", "Cartera"),
	SERVICES("Services", "Сервисы", "Servisi", "Teenused", "Servicios"),
	METERS("Meters readings", "Показания счетчиков", "Skaitītāju rādījumi", "Arvestite näidud", "Lecturas de medidores"),
	BILL_ARCHIVE("Bill archive", "Архив счетов", "Rēķinu arhīvs", "Arvete arhiiv", "Archivo de las facturas")

	private String englishSpelling
	private String russianSpelling
	private String latvianSpelling
	private String estonianSpelling
	private String spanishSpelling

	PageTitleName(String englishSpelling,
	              String russianSpelling,
	              String latvianSpelling,
	              String estonianSpelling,
	              String spanishSpelling) {
		this.englishSpelling = englishSpelling
		this.russianSpelling = russianSpelling
		this.latvianSpelling = latvianSpelling
		this.estonianSpelling = estonianSpelling
		this.spanishSpelling = spanishSpelling
	}

	String getEnglishSpelling() {
		return englishSpelling
	}

	String getRussianSpelling() {
		return russianSpelling
	}

	String getLatvianSpelling() {
		return latvianSpelling
	}

	String getEstonianSpelling() {
		return estonianSpelling
	}

	String getSpanishSpelling() {
		return spanishSpelling
	}
}