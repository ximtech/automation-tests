package me.bill.testautomation.core.webapp.modules.customer

enum CustomerPageTab {
	DASHBOARD,
	WALLET,
	SERVICES,
	METERS,
	BILL_ARCHIVE
}