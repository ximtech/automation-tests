package me.bill.testautomation.core.webapp.pages


import geb.Page
import geb.module.EmailInput
import geb.module.PasswordInput
import geb.navigator.Navigator
import me.bill.testautomation.core.webapp.utils.AppUtils
import me.bill.testautomation.core.webapp.data.users.Account

import java.util.regex.Pattern

class GoogleMailPage extends Page {
    private static final String PASS_RESET_MAIL_NAME = "Bill.me account password reset"
    public static final Pattern PATTERN_PASS_RESET_LINK = ~"^https?://(stage|sandbox).app.bill.me/registration/resetPassword/"

    static url = "https://mail.google.com"

    static content = {
        enterEmail(wait: true) { $("input[type='email']").module(EmailInput) }
        enterPassword() { $("input[type='password']").module(PasswordInput) }
        nextButton(wait: true) { resolveGmailLoginButtons() }

        passwordResetMail() { $("tr.zA.zE").$("span.bog").$("span.bqe", text: contains(PASS_RESET_MAIL_NAME)).first() }//finds only unopened latest password reset mail
        passwordResetLink(wait: true) { $("a", text: startsWith(PATTERN_PASS_RESET_LINK)).first() }
    }

    void "Sign up to google mail account"(Account account) {
        enterEmail << account.username
        nextButton.click()
        waitFor { enterPassword << account.password }
        nextButton.click()
    }

    void "Open reset password incoming message"() {
        assert waitFor("verySlow") { passwordResetMail.isDisplayed() } : "Mail with reset link is not received"
        passwordResetMail.click()
    }

    void "Click on link for password reset"() {
        passwordResetLink.click()
        driver.switchTo().window(driver.windowHandles[1])   //switch to next tab for continue
    }

    private Navigator resolveGmailLoginButtons() {
        if (AppUtils.isHeadlessModeOn()) {
            def inputNext = $("input#next")
            return inputNext.isDisplayed() ? inputNext : $("input#signIn")
        }
        return $("span.RveJvd.snByac").first()
    }
}
