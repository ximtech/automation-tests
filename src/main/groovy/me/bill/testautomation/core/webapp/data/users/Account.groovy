package me.bill.testautomation.core.webapp.data.users

import groovy.transform.AutoClone
import groovy.transform.TupleConstructor

@AutoClone
@TupleConstructor
abstract class Account {
    String username = ""
    String password = ""
}
