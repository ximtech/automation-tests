package me.bill.testautomation.core.webapp.pages.customer

import me.bill.testautomation.core.webapp.pages.PageLanguage
import geb.module.PasswordInput
import geb.module.TextInput
import me.bill.testautomation.core.webapp.utils.NavigatorUtils
import org.openqa.selenium.By

class CustomerSettingsPage extends CustomerPage {

	static url = "customerUser/profile"

	static at = { $("h2.chi-content-title").isDisplayed() }

	static content = {
		pageTitle(wait: true) { $("h2.chi-content-title") }
		languageBar(wait: true) { $(By.id("profileForm")).$("div.dropdown.pointer").children("span") }
		phoneNumberField() { $("input#phoneNumber").module(TextInput) }
		changePasswordBlock() { $("div.controls-block").first().children("a") }
		newPassword(wait: true) { $("input#passwordNew").module(PasswordInput) }
		repeatPassword(wait: true) { $("input#passwordRepeat").module(PasswordInput) }
		saveButtons() { $("button.main-button.orange-button") }
		settingsTabs() { $("div.content-tab-label") }
		checkBoxes() { $("input", type: "checkbox") }
	}

	void "Go to Personal Data"() {
		settingsTabs[0].click()
	}

	void "Go to Notifications"() {
		settingsTabs[1].click()
	}

	void "Change page language"(PageLanguage pageLang) {
		if (languageBar.text() != pageLang.getLangName()) {
			languageBar.click()
			$("div.dropdown-menu")
					.find("a.dropdown-item", text: pageLang.getLangName())
					.click()
		}
	}

	void "Enter phone number"(String phoneNumber) {
		NavigatorUtils.clearInputIfNotEmpty(phoneNumberField)
		phoneNumberField << phoneNumber
	}

	void "Click on password change block"() {
		changePasswordBlock.click()
	}

	void "Enter new password"(String password) {
		NavigatorUtils.clearInputIfNotEmpty(newPassword)
		newPassword << password
	}

	void "Confirm new password"(String password) {
		NavigatorUtils.clearInputIfNotEmpty(repeatPassword)
		repeatPassword << password
	}

	void "Check all notification checkboxes"() {
		NavigatorUtils.checkAllCheckboxes(checkBoxes)
	}

	void "Uncheck all notification checkboxes"() {
		NavigatorUtils.uncheckAllCheckboxes(checkBoxes)
	}

	void "Save phone number"() {
		saveButtons[0].click()
	}

	void "Click to save new password button"() {
		saveButtons[1].click()
	}

	void "Click to save notifications button"() {
		saveButtons[2].click()
	}

	@Override
	void "Validate page title"(String pageTitleName) {
		assert pageTitle.text() == pageTitleName: "Customer settings page title is inccorect: [${pageTitle.text()}] is not equals [${pageTitleName}]"
	}

}
