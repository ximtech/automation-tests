package me.bill.testautomation.core.webapp.data.users

class CustomerAccount extends Account {
    String email
    String address
    String firstName
    String lastName
    String phoneNumber

    String getFullName() {
        return firstName + " " + lastName
    }
}
