package me.bill.testautomation.core.webapp.pages.login


import geb.Page
import geb.module.TextInput
import me.bill.testautomation.core.webapp.pages.PageLanguage

class ForgetPasswordPage extends Page {

    static url = "registration/forgotPassword?lang=en"

    static at = {
        title == "Bill.me"
        $("a.logo").isDisplayed()
    }

    static content = {
        languageBar(wait: true) { $("div.current-language").find("span") }
        userNameField(wait: true) { $("input#j_username").module(TextInput) }
        sendTokenToUser() { $("button.btn.ng-scope") }
        checkThatMailHasBeenSend() { $("h2.auth-content__success-message.ng-scope") }
    }

    void "Set forget password page language"(PageLanguage pageLang) {
        if(languageBar.text() != pageLang.getLangShortName()) {
            languageBar.click()
            $("language-selector")
                    .find("div.select", text: pageLang.getLangName())
                    .click()
        }
    }

    void "Request for password change"(String userName) {
        "Enter user name"(userName)
        "Click on submit button"()
        "Validate that message is send to customer"()
    }

    void "Enter user name"(String name) {
        userNameField << name
    }

    void "Click on submit button"() {
        sendTokenToUser.click()
    }

    void "Validate that message is send to customer"() {
        assert waitFor { checkThatMailHasBeenSend.isDisplayed() }: "Password reset request is not send to user"
    }
}
