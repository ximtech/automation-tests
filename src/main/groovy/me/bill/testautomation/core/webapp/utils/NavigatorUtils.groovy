package me.bill.testautomation.core.webapp.utils

import geb.navigator.Navigator
import org.apache.commons.lang3.StringUtils
import org.openqa.selenium.WebElement

class NavigatorUtils {

	static void clearInputIfNotEmpty(Navigator navigator) {
		if (navigator != null && StringUtils.isNotBlank(navigator.value() as String)) {
			navigator.firstElement().clear()
		}
	}

	static void checkAllCheckboxes(Navigator navigator) {
		setAllCheckboxValues(navigator, true)
	}

	static void uncheckAllCheckboxes(Navigator navigator) {
		setAllCheckboxValues(navigator, false)
	}

	private static void setAllCheckboxValues(Navigator navigator, boolean checkAll) {
		if (navigator == null || navigator.allElements().isEmpty()) {
			return
		}
		for (WebElement element : navigator.allElements()) {
			if (checkAll) {
				if (!element.isSelected()) {
					element.click()
				}
			} else {
				if (element.isSelected()) {
					element.click()
				}
			}
		}
	}

}
