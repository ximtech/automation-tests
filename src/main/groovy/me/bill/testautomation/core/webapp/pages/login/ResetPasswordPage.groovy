package me.bill.testautomation.core.webapp.pages.login

import geb.Page
import geb.module.PasswordInput

class ResetPasswordPage extends Page {

    static url = "registration/resetPassword/"

    static at = {
        title == "Bill.me"
        $("a.logo").isDisplayed()
        $("reset-password").isDisplayed()
    }

    static content = {
        enterFirstPassword() { $("input#password").module(PasswordInput) }
        enterSecondPassword() { $("input#password2").module(PasswordInput) }
        savePassword(wait: true) { $("button.btn.ng-scope") }
        passwordHasBeenSaved() { $("h2.auth-content__success-message.ng-binding") }
    }

    void "Reset password with new data"(String password) {
        "Enter password"(password)
        "Repeat password"(password)
        "Click to save password"()
        "Check that password successfully updated"()
    }

    void "Enter password"(String password) {
        waitFor { enterFirstPassword << password }
    }

    void "Repeat password"(String nextPassword) {
        enterSecondPassword << nextPassword
    }

    void "Click to save password"() {
        savePassword.click()
    }

    void "Check that password successfully updated"() {
        assert waitFor { passwordHasBeenSaved.isDisplayed() }: "Password is not updated"
    }

}
