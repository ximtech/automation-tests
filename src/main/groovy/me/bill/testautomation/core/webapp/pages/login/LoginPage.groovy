package me.bill.testautomation.core.webapp.pages.login


import geb.Page
import geb.module.PasswordInput
import geb.module.TextInput
import me.bill.testautomation.core.webapp.data.users.Account
import me.bill.testautomation.core.webapp.pages.PageLanguage

class LoginPage extends Page {

    static url = "login/auth?lang=en"

    static at = {
        title == "Bill.me"
        $("a.logo").isDisplayed()
    }

    static content = {
        languageBar(wait: true) { $("span.language.ng-scope").first() }
        userNameInput(wait: true) { $("input#username").module(TextInput) }
        passwordInput() { $("input#password").module(PasswordInput) }
        signInButton() { $("button#loginFormSubmit") }
    }

    void "Set login page language"(PageLanguage pageLang) {
        if (languageBar.value() != pageLang.getLangName()) {
            languageBar.click()
            $("language-selector")
                    .closest("div")
                    .find("span", text: pageLang.getLangName())
                    .click()
        }
    }

    void "Sign up"(Account account) {
        userNameInput << account.username
        passwordInput << account.password
        signInButton.click()
    }
}
