import me.bill.testautomation.core.webapp.factory.DriverFactory

baseUrl = "http://localhost:8080/homepay/"
driver = { return DriverFactory.configureChromeDriver() }

waiting {
	timeout = 5
	retryInterval = 0.5
	includeCauseInMessage = true

	presets {
		quick {
			timeout = 2
			retryInterval = 0.1
		}

		slow {
			timeout = 10
			retryInterval = 1
		}

		verySlow {
			timeout = 20
			retryInterval = 2
		}
	}
}

environments {
	// run test by environment: gradle envName + "Test" (Example: stageTest, stageHeadlessTest)
	sandbox {
		baseUrl = "https://sandbox.app.bill.me/"
	}

	sandboxHeadless {
		baseUrl = "https://sandbox.app.bill.me/"
		driver = { return DriverFactory.configureHeadlessChromeDriver() }
	}

	sandboxMobile {
		baseUrl = "https://sandbox.app.bill.me/"
		driver = { return DriverFactory.configureMobileChromeDriver() }
	}

	stage {}

	stageHeadless {
		driver = { return DriverFactory.configureHeadlessChromeDriver() }
	}

	stageMobile {
		driver = { return DriverFactory.configureMobileChromeDriver() }
	}

	dev {
		baseUrl = "http://localhost:8080/homepay/"
	}

	devHeadless {
		baseUrl = "http://localhost:8080/homepay/"
		driver = { return DriverFactory.configureHeadlessChromeDriver() }
	}
}

cacheDriverPerThread = true
quitCacheDriverOnShutdown = true

reportOnTestFailureOnly = true
reportsDir = new File("report/test-report")

requirePageAtCheckers = false
atCheckWaiting = true
autoClearCookies = false